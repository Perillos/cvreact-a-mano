
import Home from './page/Home';
import NewStudis from './page/NewStudis';
import NewExperience from './page/NewExperience';
import {  Link, Route, Routes, useNavigate } from 'react-router-dom';
import './App.scss';
import Buttoncito from './components/Buttoncito';
import { useState } from 'react';
import Login from './page/Login';
import { authenticateUser } from './auth/PrivateRouter';
import PrivateRoute from './auth/PrivateRouter';
import { useDispatch, useSelector } from 'react-redux';
import { userLogIn, userLogOut } from './redux/autenticate/autenticate.action';

function App() {
  const dispatch = useDispatch()
  const navigate = useNavigate()


  const loginUser = (user) => {
    const userLogged = authenticateUser(user)
    if (userLogged) {
      dispatch(userLogIn())
      navigate('/')
    }
}

  const {userStat} =useSelector(state => state.userStat)
  console.log(userStat);
  return (
    <div className="app">
      <Link to='/'>
        <Buttoncito >Home</Buttoncito>
      </Link>
      {userStat && 
      <Link to='/newStudis'>
        <Buttoncito >Nuevos Estudios</Buttoncito>
      </Link>
      }
      {userStat && 
      <Link to='/newExperience'>
        <Buttoncito >Nuevas Experiencias</Buttoncito>
      </Link>
      }
      {!userStat &&
        <Link to='/login'>
          <Buttoncito >Login</Buttoncito>
        </Link>
      } 
      {userStat && 
          <Buttoncito exe={() => dispatch(userLogOut())}>Logout</Buttoncito>
      } 
      <Routes>
        <Route path='/' element={<Home userAuth={userStat}/>} />
        <Route path='/newStudis' element={<PrivateRoute component={<NewStudis />} />} />
        <Route path='/newExperience' element={<PrivateRoute component={<NewExperience />} />} />
        <Route path='/login' element={<Login loginToUser={loginUser}/>} />
      </Routes>
    </div>
  );
}

export default App;
