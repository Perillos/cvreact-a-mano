import { useDispatch, useSelector } from "react-redux"
import { Navigate, useNavigate } from "react-router-dom"
import { users } from '../api/users'




export const authenticateUser = (userIn) => {
    const result = users.find(
        user => user.admin === userIn.admin 
        && user.pass === userIn.pass
        )
        
        if(result) {
            return true
        }
        return false
}

    

const PrivateRoute = ({ component }) => {
    const {userStat} =useSelector(state => state.userStat)
    console.log(userStat);

    if (userStat) {
        return component
    }

    if (!userStat) {
        return <Navigate to='/login' />
    }
}

export default PrivateRoute