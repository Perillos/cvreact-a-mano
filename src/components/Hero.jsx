import Subtitle from "./Subtitle";
import Title from "./Title";

const Hero = (props) => {
    const myName = props.title
    const subTitle = props.subTitle
    
  return (
    <div>
      <Title title={myName}/>
      <Subtitle subtitle={subTitle}/>
    </div>
  );
};

export default Hero;
