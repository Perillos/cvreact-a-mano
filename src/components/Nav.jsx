import Buttoncito from "./Buttoncito";



const Nav = () => {
    const homeBut = 'Home'
    const newExpBut = 'New Experience'
    const newSpBut = 'New Estudies'

  return (
    <div>
      <Buttoncito name={homeBut}/>
      <Buttoncito name={newExpBut}/>
      <Buttoncito name={newSpBut}/>
    </div>
  );
};


export default Nav