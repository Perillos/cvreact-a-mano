import { useDispatch, useSelector } from "react-redux";
import Buttoncito from "../components/Buttoncito";
import Date from "../components/Date";
import Subtitle from "../components/Subtitle";
import { deleteEducation } from "../redux/studies/studies.action";


const Studis = (props) => {
  const {education} = useSelector(state => state.education)
  const dispatch = useDispatch()
  const clickDelet = (itema) => {
    dispatch(deleteEducation(itema))
  }
  const {userStat} = useSelector(state => state.userStat)
  return (
    <div>
      <div className='card'>
        {education.map((item, index) => {
          return (
            <div key={index} className='card__individual'>
              <Subtitle subtitle={item.info} />
              <Subtitle subtitle={item.place} />
              <Date time={item.begin} />
              <Date time={item.finish} />
              {userStat &&
                <div>
                  <Buttoncito exe={() => clickDelet(item)} >Eliminar</Buttoncito>
                </div>
              }
            </div>
          )
        })}
      </div>
    </div>
  );
};

export default Studis;
