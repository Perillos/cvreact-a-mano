import { useState } from "react"
import Hero from "../components/Hero"
import Img from "../components/Img"
import Experience from "./Experience"
import Studis from "./Studis"
import { CV } from "../api/data"
import Buttoncito from "../components/Buttoncito"
import { useSelector } from "react-redux"



const Home = ({userAuth}) => {
    // const {hero} = CV
    const {img, myName, mySurname} = CV.hero
    const {userStat} = useSelector(state => state.userStat)

    const [showEducation, setShowEducation] = useState(true);
    return (
        <div>
            <Img imag={img} />
            <Hero title={myName} subTitle={mySurname}/>
            <Buttoncito exe={() => setShowEducation(true)} >Estudios</Buttoncito>
            <Buttoncito exe={() => setShowEducation(false)}>Experiencia</Buttoncito>
            <div> 
                {showEducation
                ? (<Studis />)
                :(<Experience />)
                }
            </div>
        </div>


    )
}


export default Home