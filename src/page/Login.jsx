import { useState } from "react";
import Buttoncito from "../components/Buttoncito";

const Login = (props) => {
  const [form, setForm] = useState({ admin: "", pass: "" });

  const submitLogin = (eve) => {
    eve.preventDefault();
    props.loginToUser(form)
    
  };
  const handleChage = (eve) => {

    setForm({ ...form, [eve.target.name]: eve.target.value })
    
  }

  return (
    <form onSubmit={submitLogin}>
      <fieldset>
        <legend>Inicio de sesión</legend>
        <div>
          <label>Usuario</label>
          <input type='text' name='admin' onChange={handleChage} />
        </div>
        <div>
          <label>Contraseña</label>
          <input type='password' name='pass' onChange={handleChage} />
        </div>

        <div>
          <Buttoncito>Iniciar sesión</Buttoncito>
        </div>
      </fieldset>
    </form>
  );
};

export default Login;
