import { useDispatch, useSelector } from "react-redux";
import { deleteExperience} from "../redux/experience/experience.actions";
import Buttoncito from "../components/Buttoncito";
import Date from "../components/Date";
import Subtitle from "../components/Subtitle";

const Experience = (props) => {
  const {experience} = useSelector(state => state.experience)
  const dispatch = useDispatch()
  const clickDelet = (itema) => {
    dispatch(deleteExperience(itema))
  }
  const {userStat} = useSelector(state => state.userStat)
  return (
    <div>
      <div className='card'>
        {experience.map((item, index) => {
          return (
            <div key={index} className='card__individual'>
              <Subtitle subtitle={item.info} />
              <Subtitle subtitle={item.place} />
              <Date time={item.begin} />
              <Date time={item.finish} />
              {userStat &&
                <div>
                  <Buttoncito exe={() => clickDelet(item)} >Eliminar</Buttoncito>
                </div>
              }
            </div>
          )
        })}
      </div>
    </div>
  );
};

export default Experience;