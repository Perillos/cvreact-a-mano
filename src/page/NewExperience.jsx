
import { useForm } from "react-hook-form";
import { useDispatch } from "react-redux";
import { useNavigate } from "react-router-dom";
import Buttoncito from "../components/Buttoncito";
import { addExperience } from "../redux/experience/experience.actions";


const NewExperience = () => {
    // const nameNewExp = 'experiencia'
    const {register, handleSubmit} = useForm()

    const dispatch = useDispatch()
    const navigate = useNavigate()

    const onSubmit = (formData) => {
      dispatch(addExperience(formData));
      navigate('/')
    }



  return (
    <form onSubmit={handleSubmit(onSubmit)}>
      <fieldset>
      <legend>Añadir Experiencia</legend>
        <div>
          <label>Cargo</label>
          <input type='text' name='info' {...register('info')}/>
        </div>
        <div>
          <label>Empresa</label>
          <input type='text' name='place' {...register('place')}/>
        </div>
        <div>
          <label>Fecha Inicio</label>
          <input type="date" name='begin'{...register('begin')}/>
        </div>
        <div>
          <label>Fecha Inicio</label>
          <input type="date" name='finish' {...register('finish')}/>
        </div>
      </fieldset>
      <div>
        <Buttoncito >Incorporar</Buttoncito>
      </div>
    </form>
  );
};

export default NewExperience;
