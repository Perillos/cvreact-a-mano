
import { useState } from "react";
import { useDispatch } from "react-redux";
import { useNavigate } from "react-router-dom";
import Buttoncito from "../components/Buttoncito";
import { addEducation } from "../redux/studies/studies.action";



const INITIAL_FORM = {
  info: '',
  place: '',
  begin: '',
  finish: '',
}



const NewStudiExp = () => {
  const [form, setForm] = useState(INITIAL_FORM);
  const dispatch = useDispatch()
  const navigate = useNavigate()

  const handleSubmit = (eve) => {
    eve.preventDefault()
    dispatch(addEducation(form))
    navigate('/')
  }
  const handleChage = (eve) => {
    const {name, value} = eve.target
    setForm({...form, [name]:value})
  }

  return (
    <div>
      <form onSubmit={handleSubmit}>
      <fieldset>
      <legend>Añadir Educación</legend>
        <div>
          <label>Estudio</label>
          <input type='text' name='info' onChange={handleChage}/>
        </div>
        <div>
          <label>Escuela</label>
          <input type='text' name='place' onChange={handleChage}/>
        </div>
        <div>
          <label>Fecha Inicio</label>
          <input type="date" name='begin' onChange={handleChage}/>
        </div>
        <div>
          <label>Fecha Inicio</label>
          <input type="date" name='finish' onChange={handleChage}/>
        </div>
      </fieldset>
      <div>
        <Buttoncito >Incorporar</Buttoncito>
      </div>
    </form>
      
    </div>
  );
};

export default NewStudiExp;
