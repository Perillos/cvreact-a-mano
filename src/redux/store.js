import { configureStore } from "@reduxjs/toolkit";
import { useReducer } from "react";
import userStatReducer from "./autenticate/autenticate.reduce";
import experienceReducer from "./experience/experience.reducer";
import educationReducer from "./studies/studies.reduce";



const store2 = configureStore({
    reducer:{
        experience: experienceReducer,
        education: educationReducer,
        userStat: userStatReducer,
    }
})

export default store2;