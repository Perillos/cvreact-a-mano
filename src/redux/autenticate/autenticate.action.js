export const USE_LOGIN = 'USE_LOGIN'
export const USE_LOGOUT = 'USE_LOGOUT'


export const userLogIn = () => dispatch => {
    dispatch({
        type: USE_LOGIN,
    })
}

export const userLogOut = () => dispatch => {
    dispatch({
        type: USE_LOGOUT,
    })
}