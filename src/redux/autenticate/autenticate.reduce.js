import * as actions from './autenticate.action'


const INITIAL_STATE = {
    userStat: false
}

const userStatReducer = (state = INITIAL_STATE, action) => {
    switch(action.type){
        case actions.USE_LOGIN:
            return { ...state, userStat: true}
        case actions.USE_LOGOUT:
            return { ...state, userStat: false}
        default:
            return state
    }
}




export default userStatReducer