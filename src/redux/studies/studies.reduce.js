import * as actions from './studies.action'
import { CV } from '../../api/data'

const { fistStudi } = CV
const INITIAL_STATE = {
    education: fistStudi
}

const educationReducer = (state = INITIAL_STATE, action) => {
    const {education} = state
    switch(action.type) {
        case actions.ADD_EDUCATION:
            return {...state, education: [...education, action.payload]}
        case actions.DELETE_EDUCATION:
            const educationFiltered = education.filter(exp => exp !== action.payload)
            return {...state, education: [...educationFiltered]};
        default:
            return state
    }
}

export default educationReducer