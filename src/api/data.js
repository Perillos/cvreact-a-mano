// const mySexyPhoto = 'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcTYgFC11TsWF4OOh5yYyxpqSxSmc8HASCyccg&usqp=CAU'
// const myName = 'Fernando Valero'
// const mySurname = 'El mejor del mundo entero'
// const expeBut = 'Estudies'
// const stuBut = 'Experience'
// const fistStudi = {
//     info: 'Ingeniría de Obras',
//     place: 'Universidad Paco',
//     begin: 'Jun 2018',
//     finish: 'Jul 2020',
// }
// const fistExperi = {
//     info: 'Encargado de Ingeniría',
//     place: 'Paco SA',
//     begin: 'Jun 2018',
//     finish: 'Jul 2020',
// }
// 'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcTYgFC11TsWF4OOh5yYyxpqSxSmc8HASCyccg&usqp=CAU',


export const CV = {
    hero: {
        img:
        'https://www.tododxts.com/images/2020/04/29/01-karate-shotokan-deportes-adversarios-lucha-patada-puesta-de-sol_medium.jpeg',
        myName: 'Fernando Valero',
        mySurname: 'El mejor del mundo entero'
    },
    fistStudi: [
            {
            info: 'Ingeniría de Obras',
            place: 'Universidad Paco',
            begin: 'Jun 2018',
            finish: 'Jul 2020',
        },
        {
            info: 'Ingeniría Maligna',
            place: 'Clases de Maligno',
            begin: 'Jun 2017',
            finish: 'Jul 2018',
        },
    ],
    fistExperi: [
            {
            info: 'Encargado de Ingeniría',
            place: 'Paco SA',
            begin: 'Jun 2018',
            finish: 'Jul 2020',
        },
        {
            info: 'Encargado de Contable',
            place: 'Antonio SL',
            begin: 'Jun 2018',
            finish: 'Jul 2020', 
        }
    ]
}